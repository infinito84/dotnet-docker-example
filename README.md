# Example C# project with docker

## Introduction

This project is a .Net Core example with the following features:

- It uses docker
- It uses a containerized SQL Server in local
- It allows to create migrations with EntityFramework
- It allows to query and save models with EntityFramework
- It allows to query with Dapper
- It uses

## Getting Started

- Install [docker](https://docs.docker.com/engine/install/)
- Install [vscode](https://code.visualstudio.com/download)
- Install [Remote Containers](https://marketplace.visualstudio.com/items?itemName=ms-vscode-remote.remote-containers) extension
- Clone & Open the folder & Open in Container
- Apply migrations (see bellow)
- Run on Terminal ```dotnet run```

## Migrations

- Register the models in `EntityFrameworkContext.cs`
- Add migration ``` dotnet ef migrations add xxxxxx ```
- Apply migration ```dotnet ef database update```

## How to publish

- ```dotnet publish -r linux-x64 --self-contained true```
- This generates a folder at ./bin/Debug/net5.0/linux-x64/publish with contain all the libraries necessary to run the server, is not necessary to have IIS, or some server to run the application. You could run it in another linux-x64 without install anything related to dotnet, because all the necessary is in the folder.
- Inside the folder just run: ```./workspace```


## Testing

Create an employee

```
curl --location --request POST 'http://localhost:7000/Employee' \
--header 'Content-Type: application/json' \
--data-raw '{
    "name": "Sergio"
}'
```

Read employees

```
curl --location --request GET 'http://localhost:7000/Employee'
```