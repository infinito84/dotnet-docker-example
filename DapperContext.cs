
using Dapper;
using Microsoft.Extensions.Configuration;
using System.Data.SqlClient;


public class DapperContext
{
    private readonly IConfiguration _configuration;
    private readonly string _connectionString;
    public DapperContext(IConfiguration configuration)
    {
        _configuration = configuration;
        _connectionString = _configuration.GetConnectionString("DefaultConnection");
    }
    public SqlConnection CreateConnection()
        => new SqlConnection(_connectionString);
}