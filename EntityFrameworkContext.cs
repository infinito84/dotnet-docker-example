using Microsoft.EntityFrameworkCore;
using workspace.Models;

public class EntityFrameworkContext : DbContext
{
    public DbSet<Employee> Employees { get; set; }
    public EntityFrameworkContext(DbContextOptions<EntityFrameworkContext> options)
        : base(options)
    {
    }
}