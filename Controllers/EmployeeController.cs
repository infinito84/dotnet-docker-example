using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Dapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using workspace.Models;

namespace workspace.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class EmployeeController : ControllerBase
    {
        private readonly EntityFrameworkContext entityFrameworkContext;
        private readonly DapperContext dapperContext;
        

        public EmployeeController(EntityFrameworkContext entityFrameworkContext, DapperContext dapperContext)
        {
            this.entityFrameworkContext = entityFrameworkContext;
            this.dapperContext = dapperContext;
        }

        [HttpPost]
        public Employee Post([FromBody] Employee employee)
        {
            entityFrameworkContext.Employees.Add(employee);
            entityFrameworkContext.SaveChanges();
            return employee;
        }

        [HttpGet]
        public async Task<IEnumerable<Employee>> Get(){
            using (var connection = dapperContext.CreateConnection()){
                var employees = await connection.QueryAsync<Employee>(
                    "SELECT * FROM Employees"
                );
                return employees.ToList();
            }
        }
    }
}


